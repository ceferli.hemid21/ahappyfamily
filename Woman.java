import java.util.Random;

public final class Woman extends Human implements HumanCreator{
    public Woman(String name, String surname,byte iq, int year, String[][] schedule) {
        super(name, surname,iq, year, schedule);
    }

    public Woman(String name, String surname, int year) {
        super(name, surname, year);
    }

    public void makeup(){
        System.out.println("Wait til I change my makeup");
    }

    @Override
    public Human bornChild() {
        String[] name = new String[]{"Tural", "Hamid", "Ako"};
        Random rand = new Random();
        Human child;
        int randint2 = rand.nextInt(name.length);
        int randint = rand.nextInt(2);
        if (randint == 2){
            child = new Woman(name[randint], this.getSurname(),(byte)((this.getIq()+this.getFamily().getFather().getIq())/2), 2000, new String[][]{});
        }else{
            child = new Man(name[randint], this.getSurname(),(byte)((this.getIq()+this.getFamily().getFather().getIq())/2), 2000, new String[][]{});

        }
        child.setFamily(this.getFamily());
        this.getFamily().addChild(child);
        return child;
    }
}
