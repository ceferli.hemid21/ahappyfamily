import java.util.Arrays;
import java.util.Random;

public class Human {
    private String name;
    private String surname;
    private int year;
    private byte iq;
    private Family family;
    private String [][] schedule;



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public byte getIq() {
        return iq;
    }

    public void setIq(byte iq) {
        this.iq = iq;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public Family getFamily() {
        return family;
    }

    public Human(String name, String surname, byte iq, int year, String[][] schedule){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.schedule = schedule;
        this.iq  = iq;
    }
    public Human(String name, String surname, int year){
        this.name = name;
        this.surname = surname;
        this.year = year;
    }

    public void setFamily(Family family) {
        this.family = family;
    }


    public void greetPet(){
        System.out.println("Hello, " + family.getPet().getNickname());
    }
    public void describePet(){
        String trickLevel = this.family.getPet().getTrickLevel() >= 50 ? "very sly" : "almost not sly";
        System.out.println("I have an " + family.getPet().getSpecies() + " is " + family.getPet().getAge() + " years old " + trickLevel);
    }
    public String toString() {
        String[] newSchedule = Arrays.stream(this.schedule).flatMap(Arrays::stream).toArray(String[]::new);
        return "Human{name=" + this.name + ", surname=" + this.surname + ", year=" + this.year + ", iq=" + this.iq+" schedule= "+Arrays.toString(newSchedule)+"}";
    }


    public boolean feedPet(boolean isItTimeForFeeding){
        boolean feedHappened = false;
        if(isItTimeForFeeding){
            feedHappened = true;
        }else{
            Random rand = new Random();
            int randomNumber = rand.nextInt(100) + 1;
            feedHappened = this.family.getPet().getTrickLevel() > randomNumber ? true : false;
        }
        if (feedHappened){
            System.out.println("Hm... I will feed Jack's " + this.family.getPet().getNickname());
        }else {
            System.out.println("I think " + this.family.getPet().getNickname() + " is not hungry");
        }
        return feedHappened;
    }
}

