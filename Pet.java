import java.util.Arrays;

public abstract class Pet {
    private Species species;
    private String nickname;
    private int age;
    private byte trickLevel;
    private String [] habits;

    public Pet(String nickname, Species species){
        this.nickname = nickname;
        this.species = species;
    }
    public Pet(String nickname, Species species, int age, byte trickLevel, String [] habits){
        if (species == null){
            this.species = Species.UNKNOWN;
        }else {
            this.species = species;
        }
        this.nickname = nickname;
        this.age = age;
        this.habits = habits;
        this.trickLevel = trickLevel;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public byte getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(byte trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    public void setNickname(){
        nickname = "nigga";
    }


    public abstract void eat();

    public abstract void respond();
    public void foul(){
        System.out.println("I need to cover it up");
    }

    public String toString() {
        return  this.species + " {nickname= " + this.nickname + " age " + this.age + " trickLevel " + this.trickLevel + " habits " + Arrays.toString(this.habits) + " }";
    }
}

