public class Fish extends Pet{

    public Fish(String nickname, Species species) {
        super(nickname, species);
    }

    public Fish(String nickname, Species species, int age, byte trickLevel, String[] habits) {
        super(nickname, species, age, trickLevel, habits);
    }

    @Override
    public void eat() {

    }

    @Override
    public void respond() {

    }
}
