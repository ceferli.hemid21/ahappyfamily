public class RoboCat extends Pet{

    public RoboCat(String nickname, Species species) {
        super(nickname, species);
    }

    public RoboCat(String nickname, Species species, int age, byte trickLevel, String[] habits) {
        super(nickname, species, age, trickLevel, habits);
    }

    @Override
    public void eat() {

    }

    @Override
    public void respond() {

    }
}
