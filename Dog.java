public class Dog extends Pet{

    public Dog(String nickname, Species species) {
        super(nickname, species);
    }

    public Dog(String nickname, Species species, int age, byte trickLevel, String[] habits) {
        super(nickname, species, age, trickLevel, habits);
    }

    @Override
    public void eat() {

    }

    @Override
    public void respond() {

    }
}
