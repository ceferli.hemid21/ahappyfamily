public final class Man extends Human {
    public Man(String name, String surname,byte iq, int year, String[][] schedule) {
        super(name, surname,iq, year, schedule);
    }

    public Man(String name, String surname, int year) {
        super(name, surname, year);
    }

    public void repairCar(){
        System.out.println("Repairing car");
    }
}
